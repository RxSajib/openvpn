package app.BoomBoomVPN;

import android.app.Activity;


import android.content.SharedPreferences;
import android.preference.PreferenceManager;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.util.ArrayList;
import java.util.List;

public class InAppBillingActivity extends Activity implements PurchasesUpdatedListener {

    private static final String TAG = "InAppBilling";
    static final String ITEM_SKU_ADREMOVAL = "product.ad_removal";

    private Button mBuyButton;
    private String mAdRemovalPrice;
    private SharedPreferences mSharedPreferences;

    private BillingClient mBillingClient;
    SkuDetails skuDetails1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_app_billing);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Establish connection to billing client
        mBillingClient = BillingClient.newBuilder(this).setListener(this).build();
        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
                }
            }
            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });



        mBuyButton = (Button) findViewById(R.id.buyButton);

        mBuyButton.setOnClickListener(new View.OnClickListener() {
            private String price1;

            @Override
            public void onClick(View view) {
                BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                        .setSkuDetails(skuDetails1)
                        .build();
                BillingResult responseCode = mBillingClient.launchBillingFlow(InAppBillingActivity.this, flowParams);

//               BillingFlowParams flowParams = BillingFlowParams.newBuilder()
//                        .setSku(ITEM_SKU_ADREMOVAL)
//                        .setType(BillingClient.SkuType.INAPP)
//                        .build();
//               responseCode = mBillingClient.launchBillingFlow(InAppBillingActivity.this, flowParams);
            }
        });

        // Query purchases incase a user is connecting from a different device and they've already purchased the app
        queryPurchases();
        queryPrefPurchases();



    }
    private void queryPrefPurchases() {
        Boolean adFree = mSharedPreferences.getBoolean(getResources().getString(R.string.pref_remove_ads_key), false);
        if (adFree) {
            mBuyButton.setText(getResources().getString(R.string.pref_ad_removal_purchased));
            mBuyButton.setEnabled(false);
        }
    }

    private void queryPurchases() {


        List<String> skuList = new ArrayList<> ();
        skuList.add(ITEM_SKU_ADREMOVAL);
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        mBillingClient.querySkuDetailsAsync(params.build(),
                new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(BillingResult billingResult,
                                                     List<SkuDetails> skuDetailsList) {

                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
                            for (SkuDetails skuDetails : skuDetailsList) {
                                String sku = skuDetails.getSku();
                                String price = skuDetails.getPrice();
                                if (ITEM_SKU_ADREMOVAL.equals(sku)) {
//                                    price1 = price;
                                    skuDetails1 = skuDetails;
                                }
//                                        else if ("gas".equals(sku)) {
//                                            gasPrice = price;
//                                        }
                            }
                        }
                    }
                });



        //Method not being used for now, but can be used if purchases ever need to be queried in the future
//        Purchase.PurchasesResult purchasesResult = mBillingClient.queryPurchases(BillingClient.SkuType.INAPP);
//        if (purchasesResult != null) {
//            List<Purchase> purchasesList = purchasesResult.getPurchasesList();
//            if (purchasesList == null) {
//                return;
//            }
//            if (!purchasesList.isEmpty()) {
//                for (Purchase purchase : purchasesList) {
//                    if (purchase.getSku().equals(ITEM_SKU_ADREMOVAL)) {
//                        mSharedPreferences.edit().putBoolean(getResources().getString(R.string.pref_remove_ads_key), true).commit();
////                        setAdFree(true);
//                        mBuyButton.setText(getResources().getString(R.string.pref_ad_removal_purchased));
//                        mBuyButton.setEnabled(false);
//                    }
//                }
//            }
//        }

    }

    private void handlePurchase(Purchase purchase) {
        if (purchase.getSku().equals(ITEM_SKU_ADREMOVAL)) {
            mSharedPreferences.edit().putBoolean(getResources().getString(R.string.pref_remove_ads_key), true).commit();
//            setAdFree(true);
            mBuyButton.setText(getResources().getString(R.string.pref_ad_removal_purchased));
            mBuyButton.setEnabled(false);
        }
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {
        //If response code is OK,  handle the purchase
        //If user already owns the item, then indicate in the shared prefs that item is owned
        //If cancelled/other code, log the error

        if (BillingClient.BillingResponseCode.OK == billingResult.getResponseCode()
                && purchases != null) {
            for (Purchase purchase : purchases) {
                handlePurchase(purchase);
            }
        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
            // Handle an error caused by a user cancelling the purchase flow.
            Log.d(TAG, "User Canceled" + billingResult.getResponseCode());
        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
            mSharedPreferences.edit().putBoolean(getResources().getString(R.string.pref_remove_ads_key), true).commit();
//            setAdFree(true);
            mBuyButton.setText(getResources().getString(R.string.pref_ad_removal_purchased));
            mBuyButton.setEnabled(false);
        } else {
            Log.d(TAG, "Other code" + billingResult.getResponseCode());
            // Handle any other error codes.
        }
    }

}
